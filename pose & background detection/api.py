
from flask_restful import Api, Resource
from flask import Flask, request, Response
import flask
import PIL
from PIL import Image
import binascii
from PIL import ImageDraw
from PIL import ImageFont
import jsonpickle
import numpy as np
import cv2
import subprocess
import io
import image
from array import array
app = Flask(__name__)
api = Api(app)


class Image(Resource):
    def post(self):


        # image = Image.open(io.BytesIO(r))
        # image.save("lena.jpg")
        # img = flask.request.files.get("")
        # print(imagefile)
        # convert string of image data to uint8
        r = request
        print(r.data)
        # bin_data = binascii.unhexlify(r)
        # print(bin_data)
        # convert string of image data to uint8
        nparr = np.fromstring(r.data, np.uint8)
        print(nparr)
        # # decode image
        img = cv2.imdecode(nparr, cv2.IMREAD_COLOR)
        #
        # # do some fancy processing here....
        cv2.imwrite('lena.png', img)
        #
        # # build a response dict to send back to client
        response = {'message': 'image received. size={}x{}'.format(img.shape[1], img.shape[0])
                    }
        # encode response using jsonpickle
        response_pickled = jsonpickle.encode(response)

        # return Response(response=response_pickled, status=200, mimetype="application/json")
        p = subprocess.Popen(["python3" , 'inference.py', "lena.png"], stdout=subprocess.PIPE)
        print(p.communicate())

        p = subprocess.Popen(["python3", 'compare.py', "origin.png","lena.png"], stdout=subprocess.PIPE)

        out=p.communicate()[0].decode('utf-8')

        print(out[:-1])
        p = subprocess.Popen(["python3", 'posecompare.py'], stdout=subprocess.PIPE)

        out2 = p.communicate()[0].decode('utf-8')
        f=open("result.txt","r")
        res=f.readline()
        # build a response dict to send back to client
        return {"back": out,"pose":res}

# api 라우팅 부분
api.add_resource(Image, '/image')


if __name__ == '__main__':
    app.run(debug=True,host="0.0.0.0",port=3000)
