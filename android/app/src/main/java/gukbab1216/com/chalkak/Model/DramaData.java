package gukbab1216.com.chalkak.Model;


import gukbab1216.com.chalkak.R;

/**
 * Created by wee on 2018. 8. 18..
 */

public class DramaData {

    //드라마이름 ///쌈마이웨이//별에서온그대//함부로애틋하게/피노키오/상속자들/닥터이방인//미녀의탄생//태양의후예
    public static String[] nameArray = {"Fight for My Way", "My Love From the Star", "Uncontrollably Fond",
            "Pinocchio", "The Inheritors", "Doctor of Medicine", " Birth of a Beauty", "Descendants of The Sun"};

    //드라마 사진으로 바꿔야됨
    public static Integer[] drawableArray = {R.drawable.main_fightmyway, R.drawable.main_mylovefromthestar, R.drawable.main_uncontrollablyfond,
            R.drawable.main_pino, R.drawable.main_theinheritors, R.drawable.main_doctor, R.drawable.main_birth_devil, R.drawable.main_descendantsofthesun};

    //드라마 한줄설명
    public static String[] latitude = {"Despite an increasingly hostile world, Ko Dong Man and Choi Ae Ra decide not to give up. No matter what others speak of them, they together make their own way to live a happy life.",
            "An alien from another planet lands in Korea, and falls in love with a top Hallyu star.",
            "Despite an increasingly hostile world, Ko Dong Man and Choi Ae Ra decide not to give up. No matter what others speak of them, they together make their own way to live a happy life.",
            "An alien from another planet lands in Korea, and falls in love with a top Hallyu star.",
            "Despite an increasingly hostile world, Ko Dong Man and Choi Ae Ra decide not to give up. No matter what others speak of them, they together make their own way to live a happy life.",
            "An alien from another planet lands in Korea, and falls in love with a top Hallyu star.",
            "Despite an increasingly hostile world, Ko Dong Man and Choi Ae Ra decide not to give up. No matter what others speak of them, they together make their own way to live a happy life.",
            "An alien from another planet lands in Korea, and falls in love with a top Hallyu star."};

    public static Integer[] id_ = {0, 1, 2, 3, 4, 5, 6, 7};

}
