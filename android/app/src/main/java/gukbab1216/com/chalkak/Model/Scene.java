package gukbab1216.com.chalkak.Model;

public class Scene {
    public String mTitle;
    public String mThumbnailUrl;
    public int mThumbnail;
    public String mSummary;

    public Scene(String title, String thumbnailUrl, int thumbnail, String summary) {
        mTitle = title;
        mThumbnailUrl = thumbnailUrl;
        mThumbnail = thumbnail;
        mSummary = summary;
    }
}
