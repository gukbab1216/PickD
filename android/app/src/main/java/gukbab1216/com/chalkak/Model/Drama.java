package gukbab1216.com.chalkak.Model;

public class Drama {
    public String mTitle;
    public String mImageUrl;
    public int mImage;
    public String mSummary;

    public Drama(String title, String imageUrl, int image, String summary) {
        mTitle = title;
        mImageUrl = imageUrl;
        mImage = image;
        mSummary = summary;
    }
}
